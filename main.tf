//Cria as máquinas WebServer
resource "aws_security_group" "ec2-sg" {
  name        = var.ec2_sg_name
  description = "Allow all traffic from VPC CIDR and Open World SSH/HTTP to EC2"
  vpc_id      = var.vpc_id

  ingress {
    description      = "Allow all from VPC CIDR"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.vpc_cidr]
  }
  ingress {
    description      = "Allow OpenWorld SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "Allow OpenWorld HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.ec2_sg_name
    terraformed = "true"
  }
}

resource "aws_instance" "webserver" {
  ami = var.ami
  instance_type = var.instance_type
  availability_zone = var.az
  subnet_id = var.subnet_id
  associate_public_ip_address = true
  key_name = var.key_pair_name
  vpc_security_group_ids = [ aws_security_group.ec2-sg.id ]
  root_block_device {
    volume_size = "100"
  }
  tags = {
    Name = var.ec2_name
    terraformed = "true"
    Owner = "lazevedo"
    Webserver = "true"
  }
}

//Cria o database RDS
resource "aws_security_group" "rds-sg" {
  name        = var.rds_sg_name
  description = "Allow all traffic from VPC CIDR"
  vpc_id      = var.vpc_id

  ingress {
    description      = "Allow all from VPC CIDR"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.vpc_cidr]
  }
  ingress {
    description      = "Allow MySQL Port from OpenWorld Temp"
    from_port        = 3306
    to_port          = 3306
    protocol         = "TCP"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.rds_sg_name
    terraformed = "true"
  }
}

resource "aws_db_subnet_group" "default" {
  name       = "wp_default"
  subnet_ids = var.db_subnets

  tags = {
    Name = "DB Subnet Group for WP RDS"
  }
}

resource "aws_db_instance" "rds_mysql" {
  allocated_storage    = 20
  identifier = "wp-database"
  publicly_accessible = true
  db_name              = var.db_name
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = var.rds_size
  username             = var.rds_username
  password             = var.rds_password
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
  apply_immediately = true
  vpc_security_group_ids = [ aws_security_group.rds-sg.id ]
  db_subnet_group_name = aws_db_subnet_group.default.name
  tags = {
    terraformed = "true"
  }
}

//Cria o Target Group
resource "aws_lb_target_group" "webserver" {
  name     = "webserver-wp"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = var.alb_arn
  port              = "80"
  protocol          = "HTTP"


  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.webserver.arn
  }
}

resource "aws_lb_target_group_attachment" "ec2" {
  target_group_arn = aws_lb_target_group.webserver.arn
  target_id        = aws_instance.webserver.id
  port             = 80
}