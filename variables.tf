//WebServer Variables
variable "ami" {
    default = "ami-00874d747dde814fa"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "subnet_id" {}
variable "ec2_name" {
  default = "WebServer"
}
variable "key_pair_name" {}
variable "ec2_sg_name" {
  default = "webserver_sg"
}
variable "vpc_id" {}
variable "vpc_cidr" {}
variable "az" {}

//Database Variables
variable "db_name" {
    default = "wodpress_db"
}
variable "rds_size" {
  default = "db.t3.micro"
}
variable "rds_username" {
  default = "administrator"
}
variable "rds_password" {}

variable "rds_sg_name" {
  default = "wp-rds-sg"
}
variable "db_subnets" {
  type = list
}

//Target Group Variables
variable "alb_arn" {}